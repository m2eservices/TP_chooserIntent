package com.example.jeanclaude.tp_chooserintent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.net.Uri;



public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // ancien code
//        Uri webpage = Uri.parse("http://www.android.com");
//        Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
//
//
//        // String title = (String) getResources().getText(R.string.chooser_title);
//        String title = "Choisissez l'application : ";
//        Intent chooser = Intent.createChooser(webIntent, title);
//
//        // on affiche toutes les applis permettant d'ouvir unr URL (s'il y en a !)
//        if (webIntent.resolveActivity(getPackageManager()) != null) {
//            startActivity(chooser);
//        }

//

        // nouveau code
        Intent webIntent = new Intent(Intent.ACTION_SEND);
        webIntent.setType("text/plain");
        webIntent.putExtra(Intent.EXTRA_SUBJECT, "Sharing URL");
        webIntent.putExtra(Intent.EXTRA_TEXT, "http://www.android.com");
        startActivity(Intent.createChooser(webIntent, "Share URL"));

    }
}
